package algorithms;

import java.util.Scanner;

/**
 * Created by apripachkin on 12/10/15.
 */
public class WordsPrint {
    public static void main(String args[]) {
        Scanner in = new Scanner(System.in);
        int N = in.nextInt();
        String W = in.next();
        StringBuilder stringBuilder = new StringBuilder();
        for (int i = 0; i < N; i++) {
            stringBuilder.append(W);
            if (i != N - 1)
            stringBuilder.append("\n");
        }

        // Write an action using System.out.println()
        // To debug: System.err.println("Debug messages...");

        System.out.println(stringBuilder);
    }
}
