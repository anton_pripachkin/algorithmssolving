package algorithms;

import java.util.*;

/**
 * Created by apripachkin on 12/10/15.
 */
/*
NPUT:
Line 1: an integer N for the number of integers in the list.
N next lines: an integer X of the list.

OUTPUT:
The list of integers without duplicates, sorted in order of appearance.

CONSTRAINTS:
0 < N < 1000
-1000 < X < 1000

EXAMPLE:
Input
7
1
1
3
1
3
2
3
Output
1
3
2

 */
public class SortInputInts {
    public static void main(String args[]) {
        Scanner in = new Scanner(System.in);
        int N = in.nextInt();
        SortedSet<Integer> integerSortedSet = new TreeSet<Integer>();
        List<Integer> integerList = new ArrayList<Integer>();
        for (int i = 0; i < N; i++) {
            int X = in.nextInt();
            integerList.add(X);
        }
        integerSortedSet.addAll(integerList);

        // Write an action using System.out.println()
        // To debug: System.err.println("Debug messages...");

        System.out.println(integerSortedSet);
    }
}
