package algorithms;

import java.util.Scanner;

/**
 * Created by apripachkin on 12/4/15.
 */
public class SentenceEncrypt {
    /*
    INPUT:
Line 1: a text sentence.
Line 2: an integer number col.

OUTPUT:
The text grid with col columns.

CONSTRAINTS:
0 ≤ sentence length ≤ 100
0 < col ≤ 10
A text contains at least one non-whitespace character.

Input
Hello Perfect World
5
Output
Hello
Perfe
ctWor
ld
    IN :
    AaGpttaetdtoaahpcwelknre
4
OUT:
AaGp
ttae
tdto
aahp
cwel
knre

     */
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        String sentence = in.nextLine();
        int col = in.nextInt();
        in.nextLine();
        StringBuilder result = new StringBuilder();
        if (isInputCorrect(sentence, col)) {
            String[] split = sentence.split(" ");
            StringBuilder charsStringBuilder = new StringBuilder();
            for (int i = 0; i < split.length; i++) {
                charsStringBuilder.append(split[i]);
            }
            char[] charsToPrint = charsStringBuilder.toString().toCharArray();
            for (int i = 0; i < charsToPrint.length; i += col) {
                for (int j = i; j < i + col; j++) {
                    if (j < charsToPrint.length)
                        result.append(charsToPrint[j]);
                }
                result.append("\n");
            }
        }

        // Write an action using System.out.println()
        // To debug: System.err.println("Debug messages...");

        System.out.println(result);
    }

    private static boolean isInputCorrect(String sentence, int col) {
        return 0 <= sentence.length() && sentence.length() <= 100 && 0 < col && col <= 10;
    }
}
