package algorithms;

import java.math.BigInteger;
import java.util.Scanner;

/**
 * Created by apripachkin on 12/10/15.
 */
/*
INPUT:
Two integer numbers separated by a whitespace, a and b.

OUTPUT:
The greatest common divisor of a and b.

CONSTRAINTS:
0 < a,b ≤ 1000000

 */
public class GreatestCommonDivisor {
    public static void main(String args[]) {
        Scanner in = new Scanner(System.in);
        int a = in.nextInt();
        int b = in.nextInt();
        int gcd = 0;
        if (0 < a && b <= 1000000)
        gcd = BigInteger.valueOf(a).gcd(BigInteger.valueOf(b)).intValue();

        // Write an action using System.out.println()
        // To debug: System.err.println("Debug messages...");

        System.out.println(gcd);
    }
}
