package algorithms;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by apripachkin on 12/4/15.
 */
public class Panagram {
    /*
    A pangram is a sentence that uses every letter of the alphabet at least once.
Your program must indicate whether the given string is a pangram or not.
    INPUT:
S a string.

OUTPUT:
true if S is a pangram, false otherwise.

CONSTRAINTS:
S contains at least 1 character.
S contains less than 256 characters.
S contains only alphanumeric characters and spaces.

EXAMPLE:
Input
Abcde fghij klmno pqrs tuv wxyz
Output
true
     */
    public static void main(String[] args) {
        String input = "XCVB NMKl asdfghjk qwertyuiop";
        String alphabet = "abcdefghijklmnopqrstuvwxyz";
        List<Character> referenceAlphabet = new ArrayList<Character>();
        addCharsToList(referenceAlphabet, alphabet);
        if (isStringValid(input)) {
            System.out.println("input = " + input);
            String[] split = input.toLowerCase().split(" ");
            StringBuilder stringBuilder = new StringBuilder();
            for (int i = 0; i < split.length; i++) {
                stringBuilder.append(split[i]);
            }
            List<Character> inputChars = new ArrayList<Character>();
            addCharsToList(inputChars, stringBuilder.toString());
            System.out.println(hasAllChars(inputChars, referenceAlphabet));
        }
    }

    private static boolean hasAllChars(List<Character> characterList, List<Character> referenceList) {
        for (char c: referenceList) {
            if (!characterList.contains(c)) {
                return false;
            }
        }
        return true;
    }
    private static void addCharsToList(List<Character> characterList, String input) {
        for (char c: input.toCharArray()) {
            characterList.add(c);
        }
    }
    private static boolean isStringValid(String input) {
        boolean result = false;
        if (input.length() > 1 && input.length() < 256) {
            for (char c: input.toCharArray()) {
                if (!Character.isLetter(c) || ! (c == ' ')) {
                    result = false;
                }
            }
            result = true;
        }
        return result;
    }
}
