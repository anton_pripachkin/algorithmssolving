package algorithms;

/**
 * Created by apripachkin on 12/4/15.
 */
public class ReverseWords {
    public static void main(String[] args) {
        String input = "Hello World Hello dEv!L";
        String[] split = input.split(" ");
        StringBuilder result = new StringBuilder();
        for (int i = split.length - 1; i >= 0; i--) {
            String word = split[i];
            char[] reverseWordChars = new char[word.length()];
            changeReverseWordCase(word, reverseWordChars);
            result.append(reverseWordChars);
            if (i != 0) {
                result.append(" ");
            }
        }
        System.out.println("input = " + input);
        System.out.println("result = " + result);
    }

    private static void changeReverseWordCase(String word, char[] reverseWordChars) {
        for (int j = 0; j < reverseWordChars.length; j++) {
            char charAt = word.charAt(j);
            charAt = changeCharacterCase(charAt);
            reverseWordChars[j] = charAt;
        }
    }

    private static char changeCharacterCase(char charAt) {
        if (Character.isUpperCase(charAt)) {
            charAt = Character.toLowerCase(charAt);
        } else {
            charAt = Character.toUpperCase(charAt);
        }
        return charAt;
    }
}
