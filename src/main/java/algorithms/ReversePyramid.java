package algorithms;

/**
 * Created by apripachkin on 12/4/15.
 */
public class ReversePyramid {

    public static void main(String[] args) {
        int input = 7;
        StringBuilder stringBuilder = new StringBuilder();
        if (1 <= input && input <= 9) {
            for (int i = input; i > 0; i--) {
                for (int j = 0; j < i; j++) {
                    stringBuilder.append(input);
                }
                if (i != 1)
                    stringBuilder.append("\n");
            }

        }
        System.out.println(stringBuilder);
    }
}
