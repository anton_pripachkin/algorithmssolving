package algorithms;

import java.util.Scanner;

/**
 * Created by apripachkin on 12/10/15.
 */
/*
The program:
Your program must print the N first numbers of an arithmetic sequence of common difference R and started by 0.

An arithmetic sequence is a sequence of numbers such that the next term can be computed by adding a constant value R.

INPUT:
Two space separated integers N and R.

OUTPUT:
The first N integers of the arithmetic sequence of common difference R, starting with 0.

CONSTRAINTS:
0 < N < 100
0 ≤ R ≤ 100

EXAMPLE:
Input
5 2
Output
0 2 4 6 8

 */
public class NumberSequence {
    public static void main(String args[]) {
        Scanner in = new Scanner(System.in);
        int N = in.nextInt();
        int R = in.nextInt();
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < N; i++) {
            if (i == 0) {
                sb.append(i + " ");
            } else {
                sb.append(i * R);
                if (i != N - 1) sb.append(" ");
            }
        }

        // Write an action using System.out.println()
        // To debug: System.err.println("Debug messages...");

        System.out.println(sb);
    }
}
