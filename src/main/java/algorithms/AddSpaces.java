package algorithms;

/**
 * Created by apripachkin on 12/4/15.
 */
public class AddSpaces {
    public static void main(String[] args) {
        String input = "Codinggame";
        char[] chars = input.toCharArray();
        StringBuilder stringBuilder = new StringBuilder();
        for (int i = 0; i < chars.length; i++) {
            stringBuilder.append(chars[i]);
            if (i != chars.length - 1) {
                stringBuilder.append(" ");
            }
        }
        System.out.println(stringBuilder);
    }
}
