package algorithms;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 * Created by apripachkin on 12/10/15.
 */
/*
The program:
Your program must display every other line of the given text.

Firstly, your program must display the odd lines and in a second time the even lines (first line is line #1).

INPUT:
Line 1 : N the number of lines
N next lines : a line of the text

OUTPUT:
N / 2 first lines : odd lines of the text.
N / 2 next lines : even lines of the text.

CONSTRAINTS:
1 < N < 100
N is even
1 < Length of a line < 256

EXAMPLE:
Input
4
uuuu
lala
tutu
foo
Output
uuuu
tutu
lala
foo

 */
public class SwitchLines {
    public static void main(String args[]) {
        Scanner in = new Scanner(System.in);
        int N = in.nextInt();
        in.nextLine();
        List<String> stringList = new ArrayList<>();
        for (int i = 0; i < N; i++) {
            String e = in.nextLine();
            if (1 < e.length() && e.length() < 256)
                stringList.add(e);
        }
        StringBuilder odd = new StringBuilder();
        StringBuilder even = new StringBuilder();
        for (int i = 1; i <= stringList.size(); i++) {
            String str = stringList.get(i - 1) + "\n";
            if (i % 2 == 0) {
                even.append(str);
            } else {
                odd.append(str);
            }
        }

        // Write an action using System.out.println()
        // To debug: System.err.println("Debug messages...");

        System.out.println(odd + "" + even);
    }
}
