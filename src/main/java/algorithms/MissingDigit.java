package algorithms;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 * Created by apripachkin on 12/4/15.
 */
public class MissingDigit {

    /*
    //in 1
    //920683475
    // out: 1
    10
205749368
063981547
032945671
071863245
341527968
803751429
190854376
183972540
603285791
723695148

1
2
8
9
0
6
2
6
4
0
     */
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        List<Integer> ints = new ArrayList<Integer>();
        for (int i = 0; i < 10; i++) {
            ints.add(i);
        }
        StringBuilder result = new StringBuilder();
        if (0 < n && n < 1000) {
            for (int i = 0; i < n; i++) {
                String line = scanner.next();
                List<Integer> intsFromLine = getIntsFromLine(line);
                for (int j = 0; j < ints.size(); j++) {
                    Integer intToFind = ints.get(j);
                    if (!intsFromLine.contains(intToFind)) {
                        result.append("Line: " + i + ": " + intToFind + "\n");
                        break;
                    }
                }
            }
        }
        System.out.println(result);
    }

    private static List<Integer> getIntsFromLine(String line) {
        List<Integer> lineInts = new ArrayList<Integer>();
        for (int j = 0; j < line.length(); j++) {
            int numericValue = Character.getNumericValue(line.toCharArray()[j]);
            lineInts.add(numericValue);
        }
        return lineInts;
    }
}

