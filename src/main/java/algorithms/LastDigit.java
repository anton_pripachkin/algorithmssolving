package algorithms;

/**
 * Created by apripachkin on 12/4/15.
 */
public class LastDigit {
    public static void main(String[] args) {
        StringBuilder result = new StringBuilder();
        String st = Integer.toString(123);
        char c = st.charAt(st.length() - 1);
        System.out.println("c = " + c);
    }
}
