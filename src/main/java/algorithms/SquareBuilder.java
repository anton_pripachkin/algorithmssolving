package algorithms;

/**
 * Created by apripachkin on 12/4/15.
 */
public class SquareBuilder {
    public static void main(String[] args) {
        int input = 10;
        StringBuilder stringBuilder = new StringBuilder();
        for (int i = 0; i < input; i++) {
            for (int j = 0; j < input; j++) {
                if (j == 0 || j == input - 1 || i == 0 || i == input - 1) {
                    stringBuilder.append("#");
                } else {
                    stringBuilder.append(" ");
                }
            }
            stringBuilder.append("\n");
        }
        System.out.println(stringBuilder);
    }
}
